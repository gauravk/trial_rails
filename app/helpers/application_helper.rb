module ApplicationHelper
  # Returns the full title on a per-page basis.
  def full_title(page_title)
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else
      "#{base_title} | #{page_title}"
    end
  end
  
  def home_nav_active?(page_title)
    string = "class=active"
    if(page_title == "Home")
      string
    else
      ""
      end      
  end
  
  def help_nav_active?(page_title)
    string = "class=active"
    if(page_title == "Help")
      string
    else
      ""
      end      
  end
end
